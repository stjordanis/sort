# sort

Sort the contents of a text file, optionally using the NLS collate table


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## SORT.LSM

<table>
<tr><td>title</td><td>sort</td></tr>
<tr><td>version</td><td>1.5.1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-11-13</td></tr>
<tr><td>description</td><td>Sort the contents of a text file, optionally using the NLS collate table</td></tr>
<tr><td>keywords</td><td>sort, text, file</td></tr>
<tr><td>author</td><td>k4gvo -at- qsl.net (James Lynch)</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer &lt;eric -at- coli.uni-sb.de&gt;</td></tr>
<tr><td>platforms</td><td>DOS (e.g. Borland Turbo C), includes own KITTEN library version, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Sort</td></tr>
</table>
